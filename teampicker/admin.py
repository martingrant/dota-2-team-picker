from django.contrib import admin
from teampicker.models import Hero
from teampicker.models import Team


admin.site.register(Hero)
admin.site.register(Team)