from django import forms
from django.contrib.auth.models import User
from teampicker.models import Team

class UpdateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['email', 'last_name', 'username']
        
        
class AddNewTeamForm(forms.ModelForm):
    name = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'type' : 'text', 'class' : 'form-control', 'placeholder' : 'Name'}))
    description = forms.CharField(max_length=128, widget=forms.TextInput(attrs={'type' : 'text', 'class' : 'form-control', 'placeholder' : 'A description for your team...'}))
    slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    roster = forms.CharField(widget=forms.HiddenInput(), required=False)
    
    class Meta:
        model = Team
        fields = ['name', 'description', 'roster']
        exclude = ('user',)
        
        
class UpdateTeamForm(forms.ModelForm):
    roster = forms.CharField(widget=forms.HiddenInput(), required=False)
    class Meta:
        model = Team
        fields = ['name', 'description', 'roster']