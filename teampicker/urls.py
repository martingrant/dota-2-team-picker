from django.conf.urls import patterns, url
from teampicker import views

urlpatterns = patterns('',
        url(r'^$', views.index, name='index'),
        url(r'^test/$', views.test, name='test'),
        url(r'^about/$', views.about, name='about'),
        url(r'^users/$', views.userlist, name='userlist'),
        url(r'^users/(?P<user_username>[\w\-]+)/$', views.user, name='user'),
        url(r'^editaccount/$', views.editUser, name='editUser'),
        url(r'^teams/$', views.teamList, name='teamList'),
        url(r'^teams/(?P<team_slug>[\w\-]+)/$', views.teamView, name='team'),
        url(r'^teams/(?P<team_slug>[\w\-]+)/edit/$', views.editTeam, name='editTeam'),
        url(r'^search/$', views.search, name="search"),
        
        )
        
