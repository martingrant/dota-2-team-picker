from django.shortcuts import render
from django.http import HttpResponse
from teampicker.models import Hero
from teampicker.models import Team
from django.contrib.auth.models import User
from teampicker.forms import UpdateUserForm
from teampicker.forms import AddNewTeamForm
from teampicker.forms import UpdateTeamForm

from django.shortcuts import render_to_response
from django.template import RequestContext

from django.db.models import Q

def index(request):

    context_dict = {}
    
    heroes = Hero.objects.all()
    
    context_dict["heroes"] = heroes
    rowLength = 10
    context_dict["rowLength"] = rowLength
    
    
    teams = Team.objects.all()
    for team in teams:
        team.updateVoteCount()
        team.save()

    highlyRatedTeams = Team.objects.order_by('-voteCount')[:5]
    context_dict['highlyRated'] = highlyRatedTeams

    recentlyCreatedTeams = Team.objects.order_by('-created')[:5]
    context_dict['recentlyCreated'] = recentlyCreatedTeams

    if request.method == "POST":
        form = AddNewTeamForm(data=request.POST)
        if form.is_valid():
            team = form.save(commit=False)
            team.user = request.user
            team.save() 
            context_dict["form"] = form
            return teamView(request, team.name)
    else:
        form = AddNewTeamForm()
        context_dict["form"] = form
            
    if request.method == "GET":
        if 'clear' in request.GET:
            heroes = Hero.objects.all()
            context_dict["heroes"] = heroes  
        if 'carry' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Carry")
            context_dict["heroes"] = heroes  
        if 'support' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Support")
            context_dict["heroes"] = heroes        
        if 'laneSupport' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Lane Support")
            context_dict["heroes"] = heroes      
        if 'disabler' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Disabler")
            context_dict["heroes"] = heroes      
        if 'initiator' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Initiator")
            context_dict["heroes"] = heroes      
        if 'pusher' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Pusher")
            context_dict["heroes"] = heroes              
        if 'durable' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Durable")
            context_dict["heroes"] = heroes               
        if 'nuker' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Nuker")
            context_dict["heroes"] = heroes              
        if 'escape' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Escape")
            context_dict["heroes"] = heroes              
        if 'jungler' in request.GET:
            heroes = Hero.objects.filter(type__icontains="Jungler")
            context_dict["heroes"] = heroes     
            
    
    return render(request, 'teampicker/index.html', context_dict)
    

def test(request):
    
    context_dict = {}
    
    heroes = Hero.objects.all()
    
    context_dict["heroes"] = heroes
    
    return render(request, 'teampicker/test.html', context_dict)    
    

def about(request):
    return render(request, 'teampicker/about.html')
    
 
def user(request, user_username):
    context_dict = {}

    try:
        user = User.objects.get(username=user_username)
        context_dict['thisUser'] = user.username
        context_dict['lastLogin'] = user.last_login
        context_dict['dateJoined'] = user.date_joined
        
        teams = Team.objects.filter(user=user)
        context_dict['teams'] = teams
        
        if user == request.user:
            context_dict['ownAccount'] = 1

    except User.DoesNotExist:
        pass

    return render(request, 'teampicker/user.html', context_dict)
    
    
def userlist(request):
    context_dict = {}
    
    userList = User.objects.all()
    context_dict['users'] = userList
    
    return render(request, 'teampicker/userlist.html', context_dict)    
    
    
def editUser(request):
    context_dict = {}
    
    try:
        user = request.user
        context_dict['thisUser'] = user.username
        context_dict['email'] = user.email
        
        
        if request.method == "POST":
            form = UpdateUserForm(data=request.POST, instance=request.user)
            if form.is_valid():
                user = form.save(commit=False)
                user.save()
                
                context_dict["form"] = form
                
                context_dict['thisUser'] = user.username
                context_dict['email'] = user.email
        else:
            form = UpdateUserForm(instance=request.user)
            context_dict["form"] = form
            
    except User.DoesNotExist:
        pass

    return render(request, 'teampicker/edituser.html', context_dict)    
    

def teamList(request):
    context_dict = {}
    
    context_dict['teams'] = Team.objects.all()
    
    return render(request, 'teampicker/teamlist.html', context_dict)    
    
    
def teamView(request, team_slug):
    context_dict = {}
    
    try:
        team = Team.objects.get(slug=team_slug)
        context_dict['teamSlug'] = team.slug
        context_dict['teamName'] = team.name
        context_dict['teamDescription'] = team.description
        context_dict['dateCreated'] = team.created
        context_dict['dateModified'] = team.modified
        
        try:
            roster1 = team.roster.split(",")
            theRoster = []
            for x in range(0, len(roster1)):
                theRoster.append(Hero.objects.get(name=roster1[x]))
                
            context_dict['theRoster'] = theRoster
        except:
            pass
            
        context_dict['count'] = team.votes.count
        
        user = team.user
        context_dict['theUser'] = user
        
        if user == request.user:
            context_dict['ownTeam'] = 1
        
        if request.method == "GET":
            if request.user.is_authenticated():
                context_dict['voted'] = team.votes.exists(request.user)
            
                if 'voteUp' in request.GET:
                    if not team.votes.exists(request.user):
                        team.votes.up(request.user)
                        context_dict['votedUp'] = "1"
                        
                if 'voteDown' in request.GET:
                    team.votes.down(request.user)
                    
                if 'delete' in request.GET:
                    team.delete();    
                    return teamList(request)
                    
                    
                if team.votes.exists(request.user):
                    context_dict['votedUp'] = "1"
            
    except Team.DoesNotExist:
        pass
    
    return render(request, 'teampicker/team.html', context_dict)    
    
    
def handler404(request):
    response = render_to_response('teampicker/404.html', {},
                                  context_instance=RequestContext(request))
    response.status_code = 404
    return response
    
    
def search(request):
    context_dict = {}

    context_dict['query'] = "Search for users or team builds"

    if 'q' in request.GET:
        q = request.GET.get('q')
        try: 
            teams = Team.objects.filter(Q(name__icontains=q) | Q(slug__icontains=q))
            users = User.objects.filter(username__icontains=q)
            
            context_dict['teams'] = teams
            context_dict['users'] = users
            context_dict['query'] = q

            return render(request, 'teampicker/search.html', context_dict)

        except:  
            return render(request, 'teampicker/search.html', context_dict)

    return render(request, 'teampicker/search.html', context_dict)    
    

def editTeam(request, team_slug):
    context_dict = {}

    team = Team.objects.get(slug=team_slug)
    
    heroes = Hero.objects.all()
    
    context_dict["heroes"] = heroes
    rowLength = 10
    context_dict["rowLength"] = rowLength
    
    
   
    
    try:
        if request.method == "POST":
            form = UpdateTeamForm(data=request.POST, instance=team)
            if form.is_valid():
                team = form.save(commit=False)
                team.save()
                
                context_dict["form"] = form
                
        else:
            form = UpdateTeamForm(instance=team)
            context_dict["form"] = form
                
    except:
        pass
        
        
    try:
        roster1 = team.roster.split(",")
        theRoster = []
        for x in range(0, len(roster1)):
            theRoster.append(Hero.objects.get(name=roster1[x]))
            
        context_dict['theRoster'] = theRoster
    except:
        pass    

    context_dict['teamSlug'] = team.slug
    context_dict['teamName'] = team.name
    context_dict['teamDescription'] = team.description
    

    return render(request, 'teampicker/editteam.html', context_dict)