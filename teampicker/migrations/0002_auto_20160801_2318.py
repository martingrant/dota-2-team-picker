# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('teampicker', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='created',
        ),
        migrations.RemoveField(
            model_name='team',
            name='modified',
        ),
    ]
