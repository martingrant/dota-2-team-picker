from django.db import models
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from vote.managers import VotableManager
from django.utils import timezone

class Hero(models.Model):
    name = models.CharField(max_length=128, unique=True)
    image = models.CharField(max_length=128, default="")
    type = models.CharField(max_length=128, default="")
    

    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        return self.name
        

class Team(models.Model):
    name = models.CharField(max_length=128, default="")
    description = models.CharField(max_length=128, default="")        
    slug = models.SlugField(unique=True)
    user = models.ForeignKey(User, related_name='team', default="")
    votes = VotableManager()
    voteCount = models.IntegerField(default="0")
    roster = models.CharField(max_length=300, default="")
    created     = models.DateTimeField(default=timezone.now, editable=False)
    modified    = models.DateTimeField(default=timezone.now)

    def updateVoteCount(self):
        self.voteCount = self.votes.count()

    #save function
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        if not self.id:
            self.created = timezone.now()
        self.modified = timezone.now()
        super(Team, self).save(*args, **kwargs)
    
    def __unicode__(self):  #For Python 2, use __str__ on Python 3
        return self.name
        
