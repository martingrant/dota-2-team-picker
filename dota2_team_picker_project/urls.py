from django.conf.urls import patterns, include, url
from django.contrib import admin

from django.conf.urls import (
handler400, handler403, handler404, handler500
)
handler404 = 'teampicker.views.handler404'


urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^teampicker/', include('teampicker.urls')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
)