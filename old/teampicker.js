var teamSize = 5;
var currentTeamSize = 0;

var heroArray = new Array();

var numberOfHeroes = heroes.length;

var teamArray = new Array();
 

function HeroObject(name, image)
{
    this.name = name;
    this.image = image
    this.selected = false;
}

document.onload = setUpHereos();

function setUpHereos()
{        
    for (var i = 0; i < numberOfHeroes; ++i)
    {
        newHero = new HeroObject(heroes[i][0], heroes[i][1]);
        heroArray.push(newHero);
    }
}

function testFunc(hero)
{
    if (currentTeamSize < teamSize)
    {
        var heroName = hero.getAttribute('id');
        console.log(heroName);
        
        for (var i = 0; i < numberOfHeroes; ++i)
        {
            if (heroArray[i].name == heroName && heroArray[i].selected == false)
            {
                heroArray[i].selected = true;
                
                var table = document.getElementById("team");

                var row = table.rows[0];
                var cell = row.insertCell(0);
                
                var heroImage = hero.getAttribute('src');
                
                cell.innerHTML = '<input type="image" src="' + heroImage + '" id="'+heroName+'" onclick="testFunc2(this) "width="63" height="35" border="2"/>';
                currentTeamSize++;
            }
        }
    }
}


function testFunc2(hero)
{
    console.log("yo");
    
    var table = document.getElementById("team");
    var row = table.rows[0];
    row.deleteCell(hero.parentElement.cellIndex);
    
    currentTeamSize--;
    var heroName = hero.getAttribute('id');
    for (var i = 0; i < numberOfHeroes; ++i)
    {
            if (heroArray[i].name == heroName)
            {
                heroArray[i].selected = false;
            }
    }
}


function updateTeamDisplay()
{
    var table = document.getElementById("team");

    var row = table.rows[0];
    
    for (var i = 0; i < teamArray.length; ++i)
    {
        //var cell = row.insertCell(0);
        //cell.innerHTML = "<p>heroArray[i].name</p>"
    }
    
    
}


function selectHero(hero) 
{
    if (currentTeamSize < teamSize) 
    {
        var heroName = hero.parentNode.parentNode.rowIndex;

        var table = document.getElementById("team");

        var row = table.rows[0];
        var cell = row.insertCell(0);
        
        
        //cell.innerHTML = document.getElementById("heroes").rows[heroName].cells[0].innerHTML;
        cell.innerHTML = hero.parentElement.innerHTML;
               
        cellDeselect.innerHTML = '<input class="btn btn-default" type="button" id="' + heroName + '" onclick="deselectHero(this, false)" value="Deselect" />';
      
      
        hero.disabled = true;
        currentTeamSize += 1;
    }
}


function deselectHero(row, clearAll) 
{
    

if (clearAll == false)
{
    var rowIndex = row.parentNode.parentNode.rowIndex;
    var hero = document.getElementById("team").rows[rowIndex].cells[0].innerHTML;
}
else
{
    var hero = document.getElementById("team").rows[row].cells[0].innerHTML;
    var rowIndex = row;   
}
    
    var endOfImage = ">";
    var endOfImagePosition = hero.search(endOfImage) + 2;
    var heroName = hero.substr(endOfImagePosition, hero.length);

    document.getElementById("team").deleteRow(rowIndex);

    if (document.getElementById(heroName) != null)
    {
        document.getElementById(heroName).disabled = false;
    }

    currentTeamSize -= 1;
}


function resetHeroTable() 
{
    clearOtherFilters("null");

    clearHeroTable();
    
    addTable();
}


function clearOtherFilters(currentFilter)
{
    var filters = ["Carry", "Support", "Disabler", "Initiator", "Pusher", "Durable", "Nuker", "Escape", "Jungler", "Lane Support"];

    for (var index = 0; index < filters.length; ++index)
    {
        if (filters[index] != currentFilter)
        {
            document.getElementById(filters[index]).disabled = false;
        }
    }
}


function filterHeroes(filter) 
{
    clearHeroTable();
    clearOtherFilters(filter);
    
    document.getElementById(filter).disabled = true;
    
    var filteredHeroes = new Array();
    var filteredHeroesIndex = 0;

    for (var index = 0; index < heroes.length; ++index)
    {
        for (var heroType = 0; heroType < heroes[index][2].length; ++heroType)
        {
            if (heroes[index][2][heroType] == filter)
            {
                filteredHeroes[filteredHeroesIndex] = heroes[index];
                filteredHeroesIndex++;
            }
        }
    }
    addFilteredHeroesToTable(filteredHeroes);
}


function addFilteredHeroesToTable(filteredHeroes)
{
    var table = document.getElementById("heroes");
    
    var counter = 0;
    for (var i = 0; i < filteredHeroes.length; ++i)
    {
        if (counter == 0)
            var row = table.insertRow(-1);  
            
        var hero = row.insertCell(0);
        
        hero.innerHTML = '<input type="image" src="' + filteredHeroes[i][1] + '" id="'+filteredHeroes[i][0]+'" onclick="testFunc(this)" width="63" height="35" border="2"/>';
        
        ++counter;            
            
        if (counter == 10)
            counter = 0; 
     }
}


function clearTeam()
{
    var table = document.getElementById("team");
    
    var row = table.rows[0];
    
    table.deleteRow(0);
    
    for (var i = 0; i < heroArray.length; ++i)
    {
        heroArray[i].selected = false;
    }
    
    table.insertRow(-1);
    
    currentTeamSize = 0;
}


window.onload = addTable;
function addTable()
{
    var table2 = document.getElementById("heroes");

    var counter = 0;
    for (var i = 0; i < numberOfHeroes; ++i)
    {
        if (counter == 0)
            var row = table2.insertRow(-1);  
            
        var hero = row.insertCell(-1);
        var theHero = "test";
        hero.innerHTML = '<input type="image" src="' + heroArray[i].image + '" id="'+heroArray[i].name+'" data-toggle="tooltip" title="' + heroArray[i].name + '" onclick="testFunc(this) "width="63" height="35" border="1">';
        ++counter;            
            
        if (counter == 10)
            counter = 0;                    
    }
    
}


function clearHeroTable()
{
    var table = document.getElementById("heroes");
    
    while (table.rows.length > 1) 
    {
        table.deleteRow(1);
    }
}