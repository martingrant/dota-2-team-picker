import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'dota2_team_picker_project.settings')

import django
django.setup()

from teampicker.models import Hero
from teampicker.models import Team
from django.contrib.auth.models import User


def populate():
    
    abaddon = add_hero("Abaddon", "images/abaddon.png", "Durable, Support, Carry");
    alchemist = add_hero("Alchemist", "images/alchemist.png", "Durable, Carry, Disabler");
    ancient_apparition = add_hero("Ancient Apparition", "images/ancient_apparition.png", "Support, Disabler");
    anti_mage = add_hero("Anti-Mage", "images/antimage.png", "Carry, Escape");
    axe = add_hero("Axe", "images/axe.png", "Durable, Initiator, Disabler, Jungler");
    bane = add_hero("Bane", "images/bane.png", "Disabler, Nuker, Support");
    batrider = add_hero("Batrider", "images/batrider.png", "Initiator, Disabler, Nuker, Escape");		
    beastmaster = add_hero("Beastmaster", "images/beastmaster.png", "Initiator, Disabler, Durable");
    bloodseeker = add_hero("Bloodseeker", "images/bloodseeker.png", "Carry, Jungler");
    bounty_hunter = add_hero("Bounty Hunter", "images/bounty_hunter.png", "Carry, Escape, Nuker");
    brewmaster = add_hero("Brewmaster", "images/brewmaster.png", "Carry, Durable, Initiator, Durable");
    bristleback = add_hero("Bristleback", "images/bristleback.png", "Durable, Initiator, Disabler");
    broodmother = add_hero("Broodmother", "images/broodmother.png", "Pusher, Carry, Escape");
    centaur_warrunner = add_hero("Centaur Warrunner", "images/centaur.png", "Durable, Disabler, Initiator");
    chaos_knight = add_hero("Chaos Knight", "images/chaos_knight.png", "Carry, Disabler, Durable, Pusher");
    chen = add_hero("Chen", "images/chen.png", "Support, Jungler, Pusher");
    clinkz = add_hero("Clinkz", "images/clinkz.png", "Carry, Escape");
    clockwerk = add_hero("Clockwerk", "images/clockwerk.png", "Initiator, Durable");
    crystal_maiden = add_hero("Crystal Maiden", "images/crystal_maiden.png", "Support, Disabler, Nuker, Lane Support");
    dark_seer = add_hero("Dark Seek", "images/dark_seer.png", "Initiator, Nuker, Escape");
    dazzle = add_hero("Dazzle", "images/dazzle.png", "Support, Lane Support");
    death_prophet = add_hero("Death Prophet", "images/death_prophet.png", "Pusher, Nuker, Durable");
    disruptor = add_hero("Disruptor", "images/disruptor.png", "Nuker, Support, Initiator, Disabler");
    doom = add_hero("Doom", "images/doom.png", "Durable, Carry, Nuker");
    dragon_knight = add_hero("Dragon Knight", "images/dragon_knight.png", "Carry, Durable, Disabler, Pusher");
    drow_ranger = add_hero("Drow Ranger", "images/drow_ranger.png", "Carry");
    earth_spirit = add_hero("Earth Spirit", "images/earth_spirit.png", "Carry, Nuker");
    earthshaker = add_hero("Earthshaker", "images/earthshaker.png", "Initiator, Disabler, Support, Lane Support");
    elder_titan = add_hero("Elder Titan", "images/elder_titan.png", "Initiator, Durable");
    ember_spirit = add_hero("Ember Spirit", "images/ember_spirit.png", "Carry, Nuker, Disabler, Nuker");
    enchantress = add_hero("Enchantress", "images/enchantress.png", "Support, Pusher, Durable, Jungler");
    enigma = add_hero("Enigma", "images/enigma.png", "Disabler, Initiator, Jungler, Pusher");
    faceless_void = add_hero("Faceless Void", "images/faceless_void.png", "Carry, Initiator, Disabler, Escape");
    gyrocopter = add_hero("Gyrocopter", "images/gyrocopter.png", "Disabler, Initiator, Nuker");
    huskar = add_hero("Huskar", "images/huskar.png", "Carry, Initiator, Durable");
    invoker = add_hero("Invoker", "images/invoker.png", "Carry, Nuker, Initiator, Escape");
    io = add_hero("Io", "images/io.png", "Support, Lane Support");
    jakiro = add_hero("Jakiro", "images/jakiro.png", "Nuker, Pusher, Lane Support, Disabler");
    juggernaut = add_hero("Juggernaut", "images/juggernaut.png", "Carry, Pusher");
    keeper_of_the_light = add_hero("Keeper of the Light", "images/keeper_of_the_light.png", "Lane Support, Nuker, Support");
    kunkka = add_hero("Kunkka", "images/kunkka.png", "Disabler, Initiator, Carry, Durable");
    legion_commander = add_hero("Legion Commander", "images/legion_commander.png", "Carry, Disabler");
    leshrac = add_hero("Leshrac", "images/leshrac.png", "Nuker, Pusher, Disabler, Support");
    lich = add_hero("Lich", "images/lich.png", "Support, Lane Support, Nuker");
    lifestealer = add_hero("Lifestealer", "images/lifestealer.png", "Carry, Durable, Jungler, Escape");
    lina = add_hero("Lina", "images/lina.png", "Nuker, Disabler, Support");
    lion = add_hero("Lion", "images/lion.png", "Disabler, Nuker, Lane Support, Support");
    lone_druid = add_hero("Lone Druid", "images/lone_druid.png", "Carry, Durable, Pusher, Jungler");
    luna = add_hero("Luna", "images/luna.png", "Carry, Nuker");
    lycan = add_hero("Lycan", "images/lycan.png", "Carry, Jungler, Pusher, Durable");
    magnus = add_hero("Magnus", "images/magnus.png", "Initiator, Disabler, Nuker, Carry");
    medusa = add_hero("Medusa", "images/medusa.png", "Carry");
    meepo = add_hero("Meepo", "images/meepo.png", "Carry, Disabler, Initiator");
    mirana = add_hero("Mirana", "images/mirana.png", "Carry, Nuker, Disabler, Escape");
    morphling = add_hero("Morphling", "images/morphling.png", "Carry, Escape, Initiator, Nuker");
    naga_siren = add_hero("Naga Siren", "images/naga_siren.png", "Carry, Disabler, Pusher, Escape");
    natures_prophet = add_hero("Nature's Prophet", "images/natures_prophet.png", "Jungler, Pusher, Carry, Escape");
    necrophos = add_hero("Necrophos", "images/necrophos.png", "Carry, Durable");
    night_stalker = add_hero("Night Stalker", "images/night_stalker.png", "Durable, Initiator");
    nyx_assassin = add_hero("Nyx Assassin", "images/nyx_assassin.png", "Disabler, Nuker");
    ogre_magi = add_hero("Ogre Magi", "images/ogre_magi.png", "Nuker, Disabler, Durable");
    omniknight = add_hero("Omniknight", "images/omniknight.png", "Durable, Lane Support, Support");
    oracle = add_hero("Oracle", "images/oracle.png", "Support, Lane Support, Nuker");
    outworld_devourer = add_hero("Outworld Devourer", "images/outworld_devourer.png", "Carry");
    phantom_assassin = add_hero("Phantom Assassin", "images/phantom_assassin.png", "Carry, Escape");
    phantom_lancer = add_hero("Phantom Lancer", "images/phantom_lancer.png", "Carry, Escape, Pusher");
    pheonix = add_hero("Pheonix", "images/pheonix.png", "Initiator, Disabler, Nuker");
    pick = add_hero("Puck", "images/puck.png", "Initiator, Nuker, Disabler, Escape");
    pudge = add_hero("Pudge", "images/pudge.png", "Durable, Disabler");
    pugna = add_hero("Pugna", "images/pugna.png", "Nuker, Pusher, Support")
    queen_of_pain = add_hero("Queen of Pain", "images/queenofpain.png", "Nuker, Escape, Carry");
    razor = add_hero("Razor", "images/razor.png", "Carry, Durable, Nuker");
    riki = add_hero("Riki", "images/riki.png", "Carry, Escape");
    rubick = add_hero("Rubick", "images/rubick.png", "Disabler, Pusher");
    sand_king = add_hero("Sand King", "images/sand_king.png", "Initiator, Disabler, Nuker");
    shadow_demon = add_hero("Shadow Demon", "images/shadow_demon.png", "Support, Disabler, Nuker");
    shadow_fiend = add_hero("Shadow Fiend", "images/shadow_fiend.png", "Carry, Nuker");
    shadow_shaman = add_hero("Shadow Shaman", "images/shadow_shaman.png", "Pusher, Disabler, Nuker, Support");
    silencer = add_hero("Silencer", "images/silencer.png", "Support, Carry, Initiator");
    skywrath_mage = add_hero("Skywrath Mage", "images/skywrath_mage.png", "Nuker, Support");
    slardar = add_hero("Slardar", "images/slardar.png", "Carry, Durable, Disabler, Initiator");
    slark = add_hero("Slark", "images/slark.png", "Escape");
    sniper = add_hero("Sniper", "images/sniper.png", "Carry");
    spectre = add_hero("Spectre", "images/spectre.png", "Carry, Durable");
    spirit_breaker = add_hero("Spirit Breaker", "images/spirit_breaker.png", "Durable, Carry, Initiator, Disabler");
    storm_spirit = add_hero("Storm Spirit", "images/storm_spirit.png", "Carry, Initiator, Escape, Disabler");
    sven = add_hero("Sven", "images/sven.png", "Disabler, Initiator, Carry, Support");
    techies = add_hero("Techies", "images/techies.png", "");
    templar_assassin = add_hero("Templar Assassin", "images/templar_assassin.png", "Carry, Escape");
    terrorblade = add_hero("Terrorblade", "images/terrorblade.png", "Carry");
    tidehunder = add_hero("Tidehunter", "images/tidehunter.png", "Initiator, Durable, Disabler, Support");
    timbersaw = add_hero("Timbersaw", "images/timbersaw.png", "Durable, Nuker, Escape");
    tinker = add_hero("Tinker", "images/tinker.png", "Nuker, Pusher");
    tiny = add_hero("Tiny", "images/tiny.png", "Disabler, Nuker, Initiator, Durable");
    treant_protector = add_hero("Treant Protector", "images/treant.png", "Durable, Initiator, Lane Support, Disabler");
    troll_warlord = add_hero("Troll Warlord", "images/troll_warlord.png", "Carry");
    tusk = add_hero("Tusk", "images/tusk.png", "Initiator, Durable");
    undying = add_hero("Undying", "images/undying.png", "Durable, Pusher, Disabler, Initiator");
    ursa = add_hero("Ursa", "images/ursa.png", "Carry, Jungler, Durable");
    vengeful_spirit = add_hero("Vengeful Spirit", "images/vengeful_spirit.png", "Support, Disabler, Lane Support, Initiator");
    venomancer = add_hero("Venomancer", "images/venomancer.png", "Support, Nuker, Initiator, Pusher");
    viper = add_hero("Viper", "images/viper.png", "Carry, Durable");
    visage = add_hero("Visage", "images/visage.png", "Nuker, Durable, Disabler");
    warlock = add_hero("Warlock", "images/warlock.png", "Initiator, Support, Lane Support, Disabler");
    weaver = add_hero("Weaver", "images/weaver.png", "Carry, Escape");
    windranger = add_hero("Windranger", "images/windranger.png", "Disabler, Nuker, Support, Escape");
    winter_wyvern = add_hero("Winter Wyvern", "images/winter_wyvern.png", "");
    witch_doctor = add_hero("Witch Doctor", "images/witch_doctor.png", "Support, Disabler");
    wraith_king = add_hero("Wraith King", "images/wraith_king.png", "Durable, Carry, Disabler");
    zeus = add_hero("Zeus", "images/zeus.png", "Nuker, Support");


    #team1 = add_team("Zerg", "The best zerg team");
    #team2 = add_team("TI16", "TI16 Team");
    #team3 = add_team("Captains mode team", "My team for captains mode");

    # Print out what we have added.
    for c in Hero.objects.all():
        print "- {0}".format(str(c))




def add_hero(name, image, type):
    c = Hero.objects.get_or_create(name=name, image=image, type=type)
    return c

def add_team(name, description):
    #u = User.objects.create_user('john', 'lennon@thebeatles.com', 'johnpassword')
    u = User.objects.get(username="john")
    t = Team.objects.get_or_create(name=name, description=description, user=u)
    
    return t

# Start execution here!
if __name__ == '__main__':
    print "Starting Teampicker population script..."
    populate()