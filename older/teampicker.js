var teamSize = 5;
var currentTeamSize = 0;

function selectHero(hero) 
{
    if (currentTeamSize < teamSize) 
    {
        var heroName = hero.parentNode.parentNode.rowIndex;

        var table = document.getElementById("team");

        var row = table.insertRow(-1);
        var cellImage = row.insertCell(0);
        var cellType = row.insertCell(1);
        var cellDeselect = row.insertCell(2);

        cellImage.innerHTML = document.getElementById("heroes").rows[heroName].cells[0].innerHTML;
        cellType.innerHTML = document.getElementById("heroes").rows[heroName].cells[1].innerHTML;
        cellDeselect.innerHTML = '<input id="1btn" type="button" onclick="deselectHero(this)" value="Deselect" />';
        
        var endOfImage = ">";
        var endOfImagePosition = cellImage.innerHTML.search(endOfImage) + 2;
        var heroName = cellImage.innerHTML.substr(endOfImagePosition, cellImage.innerHTML.length);

        cellDeselect.innerHTML = '<input id="' + heroName + '" type="button" onclick="deselectHero(this, false)" value="Deselect" />';
      
        hero.disabled = true;
        currentTeamSize += 1;
    }
}


function deselectHero(row, clearAll) 
{
    

if (clearAll == false)
{
    var rowIndex = row.parentNode.parentNode.rowIndex;
    var hero = document.getElementById("team").rows[rowIndex].cells[0].innerHTML;
}
else
{
    var hero = document.getElementById("team").rows[row].cells[0].innerHTML;
    var rowIndex = row;   
}
    
    var endOfImage = ">";
    var endOfImagePosition = hero.search(endOfImage) + 2;
    var heroName = hero.substr(endOfImagePosition, hero.length);

    document.getElementById("team").deleteRow(rowIndex);

    if (document.getElementById(heroName) != null)
    {
        document.getElementById(heroName).disabled = false;
    }

    currentTeamSize -= 1;
}


function resetHeroTable() 
{
    clearOtherFilters("null");

    clearHeroTable();
    
    addTable();
}


function clearOtherFilters(currentFilter)
{
    var filters = ["Carry", "Support", "Disabler", "Initiator", "Pusher", "Durable", "Nuker", "Escape", "Jungler", "Lane Support"];

    for (var index = 0; index < filters.length; ++index)
    {
        if (filters[index] != currentFilter)
        {
            document.getElementById(filters[index]).disabled = false;
        }
    }
}


function filterHeroes(filter) 
{
    clearHeroTable();
    clearOtherFilters(filter);
    
    document.getElementById(filter).disabled = true;
    
    var filteredHeroes = new Array();
    var filteredHeroesIndex = 0;

    for (var index = 0; index < heroes.length; ++index)
    {
        for (var heroType = 0; heroType < heroes[index][2].length; ++heroType)
        {
            if (heroes[index][2][heroType] == filter)
            {
                filteredHeroes[filteredHeroesIndex] = heroes[index];
                filteredHeroesIndex++;
            }
        }
    }
    addFilteredHeroesToTable(filteredHeroes);
}


function addFilteredHeroesToTable(filteredHeroes)
{
    var table = document.getElementById("heroes");
    
    for (var i = 0; i < filteredHeroes.length; ++i)
    {
        var row = table.insertRow(-1);
        var hero = row.insertCell(0);
        var type = row.insertCell(1);
        var selectButton = row.insertCell(2);

        hero.innerHTML = '<img src="' + filteredHeroes[i][1] + '" width="63" height="35">' + " " + filteredHeroes[i][0];
        
        for (var j = 0; j < filteredHeroes[i][2].length; j++)
        {
            type.innerHTML = type.innerHTML + filteredHeroes[i][2][j] + ", ";
        }
        
        selectButton.innerHTML = '<input id="' + filteredHeroes[i][0] + '"' + ' type="button" onclick="selectHero(this)" value="Select" />';
    }
}


function clearTeam()
{
    var table = document.getElementById("team");
    
    while (table.rows.length > 1) 
    {
        deselectHero(1, true);
    }
}


window.onload = addTable;
function addTable()
{
    var numberOfHeroes = 110;

    var table2 = document.getElementById("heroes");

    for (var i = 0; i < numberOfHeroes; i++)
    {           
        var row = table2.insertRow(-1);
        var hero = row.insertCell(0);
        var type = row.insertCell(1);
        var selectButton = row.insertCell(2);

        hero.innerHTML = '<img src="' + heroes[i][1] + '" width="63" height="35">' + " " + heroes[i][0];
        
        for (var j = 0; j < heroes[i][2].length; j++)
        {
            type.innerHTML = type.innerHTML + heroes[i][2][j] + ", ";
        }
        
        selectButton.innerHTML = '<input id="' + heroes[i][0] + '"' + ' type="button" onclick="selectHero(this)" value="Select" />';
    } 
}


function clearHeroTable()
{
    var table = document.getElementById("heroes");
    
    while (table.rows.length > 1) 
    {
        table.deleteRow(1);
    }
}