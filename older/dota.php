<?php
    // Hello world heading.
    echo "<h1>Hello, world!</h1>";
 
    // But don't feel limited. You have access to absolutely all standard PHP functions, even queries:
    $query = mysql_query('SELECT * FROM pds_core_users WHERE user_id = 1');
 
    // Or use object queries if Models are not going to be used.
    $query = $db->newQuery('SELECT * FROM pds_core_users WHERE user_id = 1');
 
    // You can build HTML right here.
    $html = "<p>Some HTML</p>";
    echo $html;
?>